fun main() {
    val places = Array(9) { ' ' }
    val winningLines = listOf(
        listOf(0,1,2), listOf(3,4,5), listOf(6,7,8),    //horz lines
        listOf(0,3,6), listOf(1,4,7), listOf(2,5,8),    //vert lines
        listOf(0,4,8), listOf(2,4,6))                   //diag lines

    for (goNumber in 0 until places.size) {
        //get the input
        println("Please choose a position 1-9 to play")
        var goPosition: Int? = null
        while (goPosition == null) {
            goPosition = readLine()?.toIntOrNull()
            if (goPosition == null || goPosition < 1 || goPosition > 9) {
                goPosition = null
                println("Please enter 1-9")
            }
            else if (places[goPosition - 1] != ' ') {
                goPosition = null
                println("That space is already taken")
            }
        }

        //place the piece
        places[goPosition - 1] = if (goNumber % 2 == 0) 'O' else 'X'

        //print the grid
        places.toList()
            .chunked(3)
            .forEach { println(it.joinToString("|", "|", "|")) }

        //find if a player has won
        for (line in winningLines) {
            var first: Char? = null
            for (position in line) {
                if (places[position] != ' ' && (first == null || places[position] == first)) {
                    first = places[position]
                }
                else {
                    first = null
                    break
                }
            }
            if (first != null) {
                println("$first has won!!")
                return
            }
        }
    }
}

fun printNumbers() {
    val lessThan20 = listOf("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen")
    val tens = listOf("twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety", "one hundred")

    for (i in 0 .. 100) {
        if (i < lessThan20.size) {
            println(lessThan20[i])
        }
        else {
            val multTen = i / 10
            val units = i % 10
            val multTenInWords = tens[multTen - 2]
            if (units == 0) {
                println(multTenInWords)
            }
            else {
                val unitsInWords = lessThan20[units]
                println("$multTenInWords-$unitsInWords")
            }
        }
    }
}